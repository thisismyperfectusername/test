from django.db import models
from django.contrib.auth.models import User


class Post(models.Model):
    title = models.CharField(max_length=200)
    author = models.ForeignKey(
        User,
        on_delete=models.CASCADE,
        related_name='post_author'
    )
    body = models.TextField()
    liked_users = models.ManyToManyField(User, blank=True)

    def __str__(self):
        return self.title

    @property
    def likes_count(self):
        return self.liked_users.count()
