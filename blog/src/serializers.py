from django.contrib.auth.models import User
from src.models import Post
from rest_framework import serializers


class UserLikeSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ['pk', 'username']


class PostsSerializer(serializers.ModelSerializer):
    author = serializers.PrimaryKeyRelatedField(
            read_only=True,
            default=serializers.CurrentUserDefault()
        )
    liked_users = UserLikeSerializer(many=True, required=False)
    likes_count = serializers.IntegerField(required=False)

    class Meta:
        model = Post
        fields = ['pk', 'title', 'body', 'author', 'liked_users', 'likes_count']
