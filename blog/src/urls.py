from django.urls import path, include
from src.views import like_post, PostViewSet
from rest_framework import routers

router = routers.DefaultRouter()
router.register(r'posts', PostViewSet, basename="Posts")

urlpatterns = [
    path('', include(router.urls)),
    path('posts/<int:post_id>/like/', like_post, name='like_post'),
]
