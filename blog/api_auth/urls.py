from django.urls import path
from rest_framework_simplejwt.views import TokenObtainPairView
from rest_registration.api.views import login, logout, register, reset_password

urlpatterns = [
    path('token/', TokenObtainPairView.as_view(), name='token_obtain_pair'),
    path('login/', login, name='login'),
    path('logout/', logout, name='logout'),
    path('register/', register, name='register'),
    path('reset_password/', reset_password, name='reset_password'),
]
