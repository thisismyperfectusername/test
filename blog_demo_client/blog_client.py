from utils import (get_random_items,
                   generate_unique_username,
                   get_random_string)
from string import ascii_letters
from settings import *
import asyncio
import random
import json


class BlogApiClient:

    def __init__(self, session):
        self.session = session
        self.token = None
        self.username = generate_unique_username()
        self.password = get_random_string(ascii_letters, 8)

    async def _post_request(self, url, headers=None, **kwargs):
        async with self.session.post(url, data=kwargs, headers=headers) as response:
            return json.loads(await response.read()), response.status

    @property
    def auth_headers(self):
        return {'Authorization': 'Bearer ' + self.token}

    async def _get_request(self, url, headers=None):
        async with self.session.get(url, headers=headers) as response:
            return json.loads(await response.read())

    async def create_user(self):
        return await self._post_request(SIGNUP_URL,
                                        username=self.username,
                                        password=self.password,
                                        password_confirm=self.password)

    async def obtain_token(self):
        response_json, _ = await self._post_request(LOGIN_URL,
                                                    username=self.username,
                                                    password=self.password)
        self.token = response_json["access"]

    async def create_posts(self):
        for _ in range(random.randint(1, MAX_POST_PER_USER)):
            await self._create_post()

    async def _create_post(self):
        post_body = get_random_string(ascii_letters)
        post_title = post_body[:100]
        return await self._post_request(POST_URL,
                                        headers=self.auth_headers,
                                        body=post_body,
                                        title=post_title)

    async def like_random_posts(self):
        posts = await self._get_request(POST_URL, headers=self.auth_headers)
        for post in get_random_items(posts, 10):
            await self._like_post(post['pk'])

    async def _like_post(self, post_pk):
        return await self._post_request(LIKE_POST_URL.format(post_pk),
                                        headers=self.auth_headers)

    async def do_stuff(self):
        _, code = await self.create_user()
        if code != 201:
            return f'Failed to create user {self.username}'
        await self.obtain_token()
        await self.create_posts()
        await self.like_random_posts()
        return f'{self.username} did everything'
