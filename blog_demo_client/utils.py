import random
from string import ascii_letters, ascii_uppercase, digits


def get_random_string(collection, max_length=500):
    return ''.join(get_random_items(collection, max_length))


def get_random_items(collection, max_length=500):
    return random.choices(collection, k=random.randint(1, max_length))


def generate_unique_username(used_usernames=set()):
    username = get_random_string(ascii_letters + ascii_uppercase + digits, 8)
    if username in used_usernames:
        return generate_unique_username()
    used_usernames.add(username)
    return username
