import asyncio
from settings import *
from blog_client import BlogApiClient
from aiohttp import ClientSession


async def run_bot(session):
    client = BlogApiClient(session)
    return await client.do_stuff()


async def main():
    tasks = []
    async with ClientSession() as session:
        for _ in range(USERS_NUMBER):
            task = asyncio.ensure_future(run_bot(session))
            tasks.append(task)
        results = await asyncio.gather(*tasks)
        print(results)

if __name__ == "__main__":
    loop = asyncio.get_event_loop()
    future = asyncio.ensure_future(main())
    loop.run_until_complete(future)
