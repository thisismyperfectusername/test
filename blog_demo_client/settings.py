USERS_NUMBER = 10
MAX_POST_PER_USER = 10
BASE_URL = "http://localhost:8000/api/v1/"
SIGNUP_URL = BASE_URL + "auth/register/"
LOGIN_URL = BASE_URL + "auth/token/"
POST_URL = BASE_URL + "posts/"
LIKE_POST_URL = POST_URL + "{}/like/"
